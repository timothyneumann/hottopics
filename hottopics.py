#!/usr/bin/env python
from __future__ import division
from pymongo import MongoClient
from datetime import datetime, timedelta
import statistics
import numpy as np
import math
from scipy.integrate import simps
import sys
# import matplotlib.pyplot as plt
# import time
from pprint import PrettyPrinter

pp = PrettyPrinter(indent=1)
destination_args = {}
destination_args['host'] = 'mongo2'
destination_args['database'] = 'app-nest-disney-audience'
destination_args['collection'] = 'daily'
# destination_args['user'] = None
destination_args['password'] = None

# Connect to Host Machine
client = MongoClient(destination_args['host'])

# Select a Database
database = client[destination_args['database']]

# Authorize if Needed
if 'user' in destination_args:
    database.authenticate(destination_args['user'], destination_args['password'])

# Select a Collection
destination = database[destination_args['collection']]
nodes = database['nodes']
trends = MongoClient('mongo2')['app-nest-disney-audience']['hot']

date = datetime.utcnow()
created_at = datetime.utcnow()

def _round(num):
    num = math.floor(num * 10000 + 0.5) / 10000
    return num

def main(total_trend_minutes, _type, date=date):
    year = date.year
    month = date.month
    day = date.day
    hour = date.hour
    minute = date.minute
    enddate_trimmed = datetime(year, month, day)
    enddate = enddate_trimmed + timedelta(hours=hour, minutes=minute)
    # normalizing window is in days
    normalizing_window = 5
    key = 'dimensions.' + _type
    targets = list(destination.find({
                                'date': enddate_trimmed, 
                                'type': _type
                                },{
                                '_id':0, 
                                key:1
                                })
                                .sort('daily', -1)
                                .limit(1000))
    # print targets
    _trend = list(trends.find({
                                'date': {
                                    '$lte': enddate, 
                                    '$gt': date - timedelta(days=10)
                                    }, 
                                'type': _type
                                })
                            .sort('score', -1)
                            .limit(1))
    
    max_trend = _trend[0]['score'] if len(_trend) > 0 else 1

    
    _trend = list(trends.find({
                                'date': {
                                    '$lte': enddate, 
                                    '$gt': date - timedelta(days=10)
                                    }, 
                                'type': _type})
                            .sort('score', 1)
                            .limit(1))
    
    min_trend = _trend[0]['score'] if len(_trend) > 0 else 1
    
    targets = map(lambda x: x['dimensions'][_type], targets)
    
    
    target_data = destination.find({
                                    key: {
                                        '$in': targets
                                        }, 
                                    'date': {
                                        '$lte': enddate_trimmed, 
                                        '$gt': enddate_trimmed - timedelta(days=normalizing_window)
                                        }
                                    })
    target_hash = {}

    for doc in target_data:
        topic = tuple(doc['dimensions'][_type]) if type(doc['dimensions'][_type]) is list else doc['dimensions'][_type]
        target_hash[topic] = {'count': [], 'median': 0, 'type': doc['type'], 'name': doc['dimensions'][_type], 'target': doc['dimensions'][_type]} if topic not in target_hash else target_hash[topic]
        target_hash[topic]['count'].append(doc['daily'])
        target_hash[topic][doc['date']] = doc

    for target in target_hash:
        topic = target_hash[target]
        daily_values = topic['count']

        while len(daily_values) < normalizing_window:
            daily_values.append(0)

        topic['median'] = float(statistics.median(daily_values))
        topic['trend_data'] = []

        for i in range(total_trend_minutes-1, -1, -1):
            trend_start_time = enddate - timedelta(minutes=i)
            trend_day = datetime(trend_start_time.year, trend_start_time.month, trend_start_time.day)
            trend_hour = str(trend_start_time.hour)
            trend_minute = str(trend_start_time.minute)
            topic['trend_data'].append(topic[trend_day]['minute'][trend_hour][trend_minute] if trend_day in topic and trend_hour in topic[trend_day]['minute'] and trend_minute in topic[trend_day]['minute'][trend_hour] else 0)

        _min = 1
        area = _round(simps(np.array(topic['trend_data'])))
        topic['area'] = area

        # USE TRIMMED AVERAGE FOR TREND
        # topic['trimmed_average'] = _round(float((sum(topic['count'])-max(daily_values) - min(daily_values)))/float((normalizing_window-2)))
        # denominator2 = (float(topic['trimmed_average']) + _min) * float(total_trend_hours)/float(24) if topic['trimmed_average'] else (float(total_trend_hours) * _min)/24
        # topic['trend2'] = _round(area/denominator2 if total_trend_hours > 1 else topic['trend_data'][0]/denominator2)

        # USE MEDIAN FOR TREND
        denominator = _round((float(topic['median']) + _min) * float(total_trend_minutes)/float(24*60*60))
        topic['denominator'] = denominator
        topic['trend'] = _round(area/denominator if total_trend_minutes > 1 else topic['trend_data'][0]/denominator)

    results = []
    for target in target_hash:
        topic = target_hash[target]
        obj = {}
        obj['data'] = {
            'type': topic['type'],
            'name': topic['name'],
            # 'target': topic['target'],
            'score': math.floor(topic['trend']*100 + 0.5) / 100,
            'window': total_trend_minutes*60*1000,
            'created_at': created_at,
            'date': date
        }
        if obj['data']['score'] > max_trend:
            max_trend = obj['data']['score']
        elif obj['data']['score'] < min_trend:
            min_trend = obj['data']['score']
        obj['trend_data'] = topic['trend_data']
        results.append(obj)

    results.sort(key=lambda x: x['data']['score'], reverse=True)

    for res in results[0:20]:
        # print res 
        if sum(res['trend_data']) >= total_trend_minutes:
            res['data']['normalized_score'] = math.floor(10000*(res['data']['score'] - min_trend)/(max_trend - min_trend) + 0.5) / 100
            trends.insert(res['data'])
            # pp.pprint(res['data'])

    return results

window = 30
types = ['keyword', 'mention', 'hashtag']
# print date
for _type in types:
    # for i in range(1440, -1, -5):
    try:
        # print _type
        # results = main(window, _type, datetime(2015, 6, 8,23,40))
        # results = main(window, _type, date-timedelta(minutes=i))
        results = main(window, _type)
    except:
        error = sys.exc_info()
        print error
        pass

# QUICKLY PLOT RESULTS:
# x_series = [i for i in range(window)]
# fig1 = plt.figure()
# fig1.suptitle('Top 10 Trends')
# for ind, res in enumerate(results[0:10]):
#     print float(res['gain'])/float(res['loss'])
#     print list(nodes.find({'_id': res['target']}))[0]['name']
#     plt.subplot(str(52)+str(ind))
#     plt.plot(x_series, res['trend_data'])
# fig2 = plt.figure()
# fig2.suptitle('Top 10 -20 Trends')
# for ind, res in enumerate(results[10:20]):
#     print float(res['gain'])/float(res['loss'])
#     print list(nodes.find({'_id': res['target']}))[0]['name']
#     plt.subplot(str(52)+str(ind % 10))
#     plt.plot(x_series, res['trend_data'])
# plt.show()
